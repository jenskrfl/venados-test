#include <iostream>
#include <stdio.h>
#include <QList>

using namespace std;

class Matrix
{

   private:
    int _columns;
    int _rows;

public:
    Matrix(int m, int n):
        _columns{m},
        _rows{n}
    {
    }

    void setData(int m, int n){

        _columns = m;
        _rows = n;
    }

    int getColum(){
        return _columns;
    }

    int getRow(){
        return _rows;
    }

    int matrixElements(){
        return _columns*_rows;
    }
};





int main()
{
    int cases = 0;
    string output;
    int rows =0;
    int columns =0;
    int state = 0;
    int matrixElement = 0;
    int aux = 2;
    QList<Matrix> list;

    cout << "How many cases do you want?\n";
    cin >> cases;

    if (cases > 500 || cases < 1){

        cout << "cases number must be between 1 and 500  ";
        cout << "How many cases do you want?\n";
        cin >> cases;
    }

    for(int i=0;i<cases;i++)
    {
        int columns, rows;
        cout << ("please enter the number of columns for the matrix "+ to_string(i+1)+ "\n");
        cin >> columns;

        while (columns < 1 || columns > 1000000000) {
            cout << "colum must be between 0 and 10^9\n";
            cout << ("please enter the number of columns for the matrix "+ to_string(i+1)+ "\n");
            cin >> columns;
        }

        cout << ("please enter the number of rows for the matrix "+to_string(i+1) + "\n");
        cin >> rows;

        while (rows < 1 || rows > 1000000000) {
            cout << "row must be between 0 and 10^9\n";
            cout << ("please enter the number of rows for the matrix "+to_string(i+1) + "\n");
            cin >> rows;
        }

        Matrix m (columns, rows);
        list.append(m);

    }

    cout <<("Facings: \n");


    for(Matrix m : list ){

        output;
       rows =0;
       columns =0;
       state = 0;
       matrixElement = 0;
       aux = 2;

      while (matrixElement < m.matrixElements() ) {

       int actualRows = m.getRow()-rows;
       int actualColums = m.getColum()-columns;

     if(state==0)
        {
           for(int newMat=rows; newMat<actualColums; newMat++)
            {
                matrixElement++;
                output = "R";
            }
              state++;
        }
      else if(state==1)
       {
         for(int newMat=rows+1;newMat<actualRows;newMat++)
          {
              matrixElement++;
              output = "D";
          }
          state++;
      }
      else if(state==2)
      {
          for(int newMat = actualColums-aux; newMat >= columns; newMat--)
          {
              matrixElement++;
              output = "L";
          }
          state++;
      }
      else if(state==3)
      {
          for (int newMat = actualRows - aux; newMat >= rows + 1; newMat--)
          {
              matrixElement++;
              output = "U";
          }
          state = 0;
          rows++;
          columns++;
      }


   }

        cout << (output+"\n");

 }


    return 0;
}



