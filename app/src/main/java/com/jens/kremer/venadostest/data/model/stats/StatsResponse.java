package com.jens.kremer.venadostest.data.model.stats;

import com.google.gson.annotations.SerializedName;

public class StatsResponse {

    @SerializedName("sucess")
    private boolean succes;
    @SerializedName("data")
    private ListStats listStats;
    @SerializedName("code")
    private int code;

    public StatsResponse() {
    }

    public StatsResponse(boolean succes, ListStats listStats, int code) {
        this.succes = succes;
        this.listStats = listStats;
        this.code = code;

    }

    public boolean isSucces() {
        return succes;
    }

    public void setSucces(boolean succes) {
        this.succes = succes;
    }

    public ListStats getData() {
        return listStats;
    }

    public void setData(ListStats listStats) {
        this.listStats = listStats;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
