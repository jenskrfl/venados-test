package com.jens.kremer.venadostest.data.remote.commons;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class RetrofitService<Object> implements Callback<Object> {

    public RetrofitService(){
        super();

    }


    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {
        onResult(response);
    }

    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        onError(t);
    }


    public abstract void onResult(Response result);
    public abstract void onError(Throwable result);

}
