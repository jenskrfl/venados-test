package com.jens.kremer.venadostest.data.model.players;

import com.google.gson.annotations.SerializedName;

public class DataTeams {
    @SerializedName("team")
    private Positions positions;

    public DataTeams() {

    }

    public DataTeams(Positions positions) {
        this.positions = positions;
    }

    public Positions getPositions() {
        return positions;
    }

    public void setPositions(Positions positions) {
        this.positions = positions;
    }
}
