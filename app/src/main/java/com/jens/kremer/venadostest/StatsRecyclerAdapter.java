package com.jens.kremer.venadostest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jens.kremer.venadostest.data.model.stats.Stats;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatsRecyclerAdapter extends RecyclerView.Adapter<StatsRecyclerAdapter.MyViewHolder>{
    private Context context;
    private ArrayList<Stats> stats=new ArrayList<Stats>();

    public StatsRecyclerAdapter(ArrayList<Stats> stats, Context context) {
        this.stats = stats;
        this.context = context;
    }


        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.item_recycler_stats, viewGroup, false);
            return new StatsRecyclerAdapter.MyViewHolder(view);

        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

            myViewHolder.bind(stats.get(i), context);

        }

        @Override
        public int getItemCount() {
            return stats.size();
        }

    static class MyViewHolder extends RecyclerView.ViewHolder  {

        @BindView(R.id.txt_team_position) TextView teamPosition;
        @BindView(R.id.img_team) ImageView teamImage;
        @BindView(R.id.txt_team_id) TextView teamId;
        @BindView(R.id.txt_team_gemes) TextView teamGames;
        @BindView(R.id.txt_team_delta_score) TextView teamDeltaScore;
        @BindView(R.id.txt_team_points) TextView teamPoints;

        private  Context context;
        private  Stats stat;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Stats stat, Context context1) {

            this.context = context1;
            this.stat = stat;

            teamPosition.setText(String.valueOf(stat.getPosition()));
            Picasso.get().load(stat.getImage()).into(teamImage);
            teamId.setText(stat.getTeam());
            teamGames.setText(String.valueOf(stat.getGames()));
            teamDeltaScore.setText(String.valueOf(stat.getScore_diff()));
            teamPoints.setText(String.valueOf(stat.getPoints()));

        }


    }


}
