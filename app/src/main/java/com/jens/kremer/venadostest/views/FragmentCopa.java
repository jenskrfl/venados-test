package com.jens.kremer.venadostest.views;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jens.kremer.venadostest.GamesRecyclerAdapter;
import com.jens.kremer.venadostest.R;
import com.jens.kremer.venadostest.data.model.games.Games;
import com.jens.kremer.venadostest.data.model.games.GamesResponse;
import com.jens.kremer.venadostest.data.remote.service.common.APIService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCopa extends Fragment {
    protected Context context;
    private GamesRecyclerAdapter gamesRecyclerAdapter;
    ArrayList<Games> mGames = new ArrayList<>();
    APIService mAPIService;
    boolean copa;
    ArrayList<Games> games;
    @BindView(R.id.recycler_games)
    RecyclerView recyclerGames;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    public FragmentCopa() {

    }

    public static FragmentCopa newInstance(String param1, String param2) {
        FragmentCopa fragment = new FragmentCopa();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_copa, container, false);
        ButterKnife.bind(this, view);

        gamesRecyclerAdapter = new GamesRecyclerAdapter(games, getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerGames.setLayoutManager(linearLayoutManager);
        recyclerGames.setAdapter(gamesRecyclerAdapter);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                gamesRecyclerAdapter = new GamesRecyclerAdapter(reloadListGames(), getActivity());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                recyclerGames.setLayoutManager(linearLayoutManager);
                recyclerGames.setAdapter(gamesRecyclerAdapter);
                swipeRefreshLayout.setRefreshing(false);

            }
        });


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }


    }

    @Override
    public void onResume() {

        super.onResume();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();


        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void setGames(ArrayList<Games> games) {
        //games.clear();
        this.games = games;
    }

    ArrayList<Games> reloadListGames() {

        mGames.clear();

        ((MainActivity)getActivity()).getdata();
        mGames.addAll(((MainActivity)getActivity()).getGamesList().getGames());

        if(mGames.isEmpty()){

            Games g = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-10-01T11:11:11+11:11", "Copa", "https://i.imgur.com/GVbzPo3.png",7,9 );
            Games g1 = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-11-01T11:11:11+11:11", "Copa", "https://i.imgur.com/GVbzPo3.png",7,4 );


            Games g2 = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-12-01T11:11:11+11:11", "Copa", "https://i.imgur.com/GVbzPo3.png",8,9 );

            Games g2x = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-11-02T11:11:11+11:11", "Copa", "https://i.imgur.com/GVbzPo3.png",8,9 );
            Games g2y = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-11-03T11:11:11+11:11", "Copa", "https://i.imgur.com/GVbzPo3.png",8,9 );

            Games g3 = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-10-01T11:11:11+11:11", "Ascenso", "https://i.imgur.com/GVbzPo3.png",0,0 );
            Games g3x = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-10-02T11:11:11+11:11", "Ascenso", "https://i.imgur.com/GVbzPo3.png",0,0 );
            Games g4 = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-11-01T11:11:11+11:11", "Ascenso", "https://i.imgur.com/GVbzPo3.png",2,0 );


            Games g5 = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
                    ,"2010-12-01T11:11:11+11:11", "Ascenso", "https://i.imgur.com/GVbzPo3.png",0,3 );


            mGames.add(g);
            mGames.add(g1);
            mGames.add(g2x);
            mGames.add(g2y);
            mGames.add(g2);
            mGames.add(g3);
            mGames.add(g3x);
            mGames.add(g4);
            mGames.add(g5);

            if(copa){return getGames(mGames, "copa");}
            else {return getGames(mGames, "Ascenso");}


        }

        return mGames;
    }

    public ArrayList<Games> getGames(ArrayList<Games> games1, String tag)
    {
        ArrayList<Games> g = new ArrayList<>();
        for(Games game: games1)
        {
            if(game.getLeague().toLowerCase().contains(tag.toLowerCase()))
            {
                g.add(game);
            }
        }

        return g;
    }
    public void setCopa(boolean copa) {
        this.copa = copa;
    }
}
