package com.jens.kremer.venadostest.data.model.stats;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class Stats {
    @SerializedName("position")
    private int position;
    @SerializedName("image")
    private String image;
    @SerializedName("team")
    private String team;
    @SerializedName("games")
    private int games;
    @SerializedName("win")
    private int win;
    @SerializedName("loss")
    private int loss;
    @SerializedName("tie")
    private int tie;
    @SerializedName("f_goals")
    private int f_goal;
    @SerializedName("a_goals")
    private int a_goals;
    @SerializedName("score_diff")
    private int score_diff;
    @SerializedName("points")
    private int points;
    @SerializedName("efec")
    @Nullable
    private String efec;

    public Stats() {
    }

    public Stats(int position, String image, String team, int games, int win, int loss, int tie, int f_goal, int a_goals, int score_diff, int points, String efec) {
        this.position = position;
        this.image = image;
        this.team = team;
        this.games = games;
        this.win = win;
        this.loss = loss;
        this.tie = tie;
        this.f_goal = f_goal;
        this.a_goals = a_goals;
        this.score_diff = score_diff;
        this.points = points;
        this.efec = efec;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win = win;
    }

    public int getLoss() {
        return loss;
    }

    public void setLoss(int loss) {
        this.loss = loss;
    }

    public int getTie() {
        return tie;
    }

    public void setTie(int tie) {
        this.tie = tie;
    }

    public int getF_goal() {
        return f_goal;
    }

    public void setF_goal(int f_goal) {
        this.f_goal = f_goal;
    }

    public int getA_goals() {
        return a_goals;
    }

    public void setA_goals(int a_goals) {
        this.a_goals = a_goals;
    }

    public int getScore_diff() {
        return score_diff;
    }

    public void setScore_diff(int score_diff) {
        this.score_diff = score_diff;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Nullable
    public String getEfec() {
        return efec;
    }

    public void setEfec(@Nullable String efec) {
        this.efec = efec;
    }
}
