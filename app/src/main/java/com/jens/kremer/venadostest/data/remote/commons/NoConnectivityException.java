package com.jens.kremer.venadostest.data.remote.commons;


import com.jens.kremer.venadostest.data.local.prefs.Constants;

import java.io.IOException;

public class NoConnectivityException extends IOException {
    @Override
    public String getMessage() {
        return Constants.noConnException;
    }

}
