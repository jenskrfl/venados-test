package com.jens.kremer.venadostest.data.model.games;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GamesList {
    //The web service response a list of games.
    @SerializedName("games")
    private ArrayList<Games> gamesList;

    public GamesList() {}

    public GamesList(ArrayList<Games> gamesList) {
        this.gamesList = gamesList;
    }

    public ArrayList<Games> getGames() {
        return gamesList;
    }

    public void setGames(ArrayList<Games> gamesList) {
        this.gamesList = gamesList;
    }
}
