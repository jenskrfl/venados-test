package com.jens.kremer.venadostest.data.remote.commons;


import com.jens.kremer.venadostest.data.local.prefs.Constants;
import com.jens.kremer.venadostest.data.remote.service.common.APIService;

import retrofit2.Retrofit;

public class ApiUtils {

    public ApiUtils(){}

    public static APIService getApiService(){
        return RetrofitClient.getClient(Constants.BASE_URL).create(APIService.class);
    }
    public static Retrofit getRetrofit(){
        return RetrofitClient.getClient(Constants.BASE_URL);
    }


}
