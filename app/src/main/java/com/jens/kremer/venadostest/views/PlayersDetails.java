package com.jens.kremer.venadostest.views;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jens.kremer.venadostest.R;
import com.jens.kremer.venadostest.data.model.players.Players;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PlayersDetails extends DialogFragment {

    Context context;

    Players player;

    @BindView(R.id.img_player_details)
    ImageView imgPlayerDetails;
    @BindView(R.id.txt_player_details_name)
    TextView txtName;
    @BindView(R.id.txt_player_details_position)
    TextView txtPosition;
    @BindView(R.id.txt_player_details_birthday)
    TextView txtBirthdayM;
    @BindView(R.id.txt_player_details_birthdayD)
    TextView txtBirthdayD;
    @BindView(R.id.txt_player_details_weight)
    TextView txtPlayerWeight;
    @BindView(R.id.txt_player_details_height)
    TextView txtPlayerHeight;
    @BindView(R.id.txt_player_details_Pteam)
    TextView txtPreviousTeam;

    public PlayersDetails() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players_details, container, false);
        ButterKnife.bind(this, view);


        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(60)
                .oval(false)
                .build();

        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");

        // Set the date to the holder
        Date theDate = null;
        try {
            theDate = simpleDateFormat.parse(player.getBirthday());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Date finalTheDate = theDate;

        Calendar myCal = new GregorianCalendar();
        myCal.setTime(theDate);

        if(player != null) {
            Picasso.get().load(player.getUrlImage()).transform(transformation).into(imgPlayerDetails);
            txtName.setText(player.getName());

            if(player.getPosition() != null) {
                txtPosition.setText(player.getPosition());
            }

            else {
                txtPosition.setText(player.getRole());

            }

            txtPlayerHeight.setText(String.valueOf(player.getHeight()));
            txtPlayerWeight.setText(String.valueOf(player.getWeight())+"KG");
            txtPreviousTeam.setText(player.getLastTeam());
            txtBirthdayM.setText(String.valueOf(myCal.get(Calendar.DATE))+"/"+String.valueOf(myCal.get(Calendar.MONTH)+1)+"/"+String.valueOf(myCal.get(Calendar.YEAR)));
            txtBirthdayD.setText(player.getBirthPlace()) ;
        }

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setPlayer(Players player) {
        this.player = player;
    }
}
