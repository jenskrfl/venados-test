package com.jens.kremer.venadostest.data.remote.service.common;

import com.jens.kremer.venadostest.data.model.games.GamesResponse;
import com.jens.kremer.venadostest.data.model.players.PlayersResponse;
import com.jens.kremer.venadostest.data.model.stats.StatsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface APIService {

    @Headers( {"Accept: application/json" })
    @GET("players")
    Call<PlayersResponse> getPlayers();

    @Headers( {"Accept: application/json" })
    @GET("statistics")
    Call<StatsResponse> getStatistics();

    @Headers( {"Accept: application/json" })
    @GET("games")
    Call<GamesResponse> getGames();

}
