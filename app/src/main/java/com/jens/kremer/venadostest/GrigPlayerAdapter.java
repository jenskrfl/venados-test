package com.jens.kremer.venadostest;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jens.kremer.venadostest.views.FragmentPlayers;
import com.jens.kremer.venadostest.views.PlayersDetails;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.jens.kremer.venadostest.data.model.players.Players;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GrigPlayerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Players> players;
    private FragmentManager fragmentManager;
    private FragmentPlayers fragmentPlayers;


    public GrigPlayerAdapter(Context context, ArrayList<Players> players, FragmentManager fragmentManager, FragmentPlayers fragmentPlayers) {
        this.fragmentPlayers = fragmentPlayers;
        this.context = context;
        this.players = players;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int i) {
        return players.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @BindView(R.id.img_player)
    ImageView imgPlayer;
    @BindView(R.id.txt_player_name)
    TextView txtName;
    @BindView(R.id.txt_player_position)
    TextView txtPosition;

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        final int index;

        if(view == null){

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_grid_players, null);
        }

        ButterKnife.bind(this, view);

        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.BLACK)
                .borderWidthDp(0)
                .cornerRadiusDp(60)
                .oval(false)
                .build();

        Picasso.get().load(players.get(i).getUrlImage()).transform(transformation).into(imgPlayer);
        txtName.setText(players.get(i).getName());
        if(players.get(i).getPosition() != null){txtPosition.setText(players.get(i).getPosition());}

        else {txtPosition.setText(players.get(i).getRole());}

         index = i;

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PlayersDetails playersDetails = new PlayersDetails();
                playersDetails.setPlayer(players.get(index));
                playersDetails.show(fragmentManager, "playersDetails");
                playersDetails.setTargetFragment(fragmentPlayers, 1);
                            }
        });

        return view;
    }
}
