package com.jens.kremer.venadostest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.jens.kremer.venadostest.data.model.games.Games;
import com.jens.kremer.venadostest.views.FragmentCopa;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<Games>  games = new ArrayList<>();
    ArrayList<Games>  gamesaux = new ArrayList<>();

    Games g1 = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
    ,"2010-11-01T11:11:11+11:11", "Copa MX", "https://i.imgur.com/GVbzPo3.png",7,9 );

    Games g2 = new Games(true, "toros", "http://asisucedegto.mx/wp-content/uploads/2015/05/306716_489956761030799_1004094830_n.jpg"
            ,"2010-11-01T11:11:11+11:11", "Ascenso", "https://i.imgur.com/GVbzPo3.png",0,0 );

   //" yyyy-MM-dd'T'HH:mm:ss+SS:SS"

    public ViewPagerAdapter(FragmentManager fm, ArrayList<Games> games) {
        super(fm);
        this.games =  games;

    }

    @Override
    public Fragment getItem(int position) {

        for(int i = 0; i < 9; i++ ){

           gamesaux.add(g1);
           gamesaux.add(g2);

        }
        switch (position) {
            case 0:
                FragmentCopa fc = new FragmentCopa();
                fc.setGames(getGames(games, "Copa"));
                fc.setCopa(true);
                return fc;
            case 1:
                FragmentCopa fc2 = new FragmentCopa();
                fc2.setGames(getGames(games, "Ascenso"));
                fc2.setCopa(false);
                return fc2;
            default:
                return null;
        }

    }


    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "COPA MX";
        }
        else if (position == 1)
        {
            title = "ASCENSO MX";
        }

        return title;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);

    }



    public ArrayList<Games> getGames(ArrayList<Games> games1, String tag)
    {
        ArrayList<Games> g = new ArrayList<>();
        for(Games game: games1)
        {
            if(game.getLeague().toLowerCase().contains(tag.toLowerCase()))
            {
                g.add(game);
            }
        }

        return g;
    }



}
