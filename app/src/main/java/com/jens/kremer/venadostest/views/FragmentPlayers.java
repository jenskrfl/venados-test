package com.jens.kremer.venadostest.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;

import com.jens.kremer.venadostest.GrigPlayerAdapter;
import com.jens.kremer.venadostest.R;
import com.jens.kremer.venadostest.data.model.players.DataTeams;
import com.jens.kremer.venadostest.data.model.players.Players;
import com.jens.kremer.venadostest.data.model.players.PlayersResponse;
import com.jens.kremer.venadostest.data.remote.commons.ApiUtils;
import com.jens.kremer.venadostest.data.remote.service.common.APIService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentPlayers extends Fragment {

    Context context;
    @BindView(R.id.btn_filter)
    Button btnFilter;
    private APIService mAPIService;
    DataTeams allPlayers;
    ArrayList<Players> centers;
    ArrayList<Players> forwards;
    ArrayList<Players> defenses;
    ArrayList<Players> goalkeepers;
    ArrayList<Players> coaches;

    @BindView(R.id.gridPlayer)
    GridView gridPlayer;
    @BindView(R.id.cat_spinner)
    Spinner spinner;
    @BindView(R.id.bar_view)
    View barView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_players, container, false);
        ButterKnife.bind(this, view);

        AppCompatActivity appCompatActivity = (AppCompatActivity) getActivity();
        if (appCompatActivity != null) {
            ActionBar actionBar = appCompatActivity.getSupportActionBar();
            if (actionBar != null)
                actionBar.setElevation(0);
        }

        final ArrayAdapter<String> adapterS = new ArrayAdapter<>(context, R.layout.spinner_item);
        spinner.setAdapter(adapterS);
        adapterS.add(getString(R.string.all));
        adapterS.add(getString(R.string.delanteros));
        adapterS.add(getString(R.string.centros));
        adapterS.add(getString(R.string.defensa));
        adapterS.add(getString(R.string.portero));
        adapterS.add(getString(R.string.coach));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                handleCategorySpinnerItemSelected(position);
                //view.setBackgroundColor(context.getColor(R.color.textColorPrimary));


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mAPIService = ApiUtils.getApiService();
        Call<PlayersResponse> playersResponse = mAPIService.getPlayers();
        playersResponse.enqueue(new Callback<PlayersResponse>() {
            @Override
            public void onResponse(Call<PlayersResponse> call, Response<PlayersResponse> response) {
                if (response.body() instanceof PlayersResponse) {

                    allPlayers = response.body().getData_teams();
                    centers = allPlayers.getPositions().getCenters();
                    forwards = allPlayers.getPositions().getForwards();
                    defenses = allPlayers.getPositions().getDefenses();
                    goalkeepers = allPlayers.getPositions().getGoalKeepers();
                    coaches = allPlayers.getPositions().getCoaches();
                    handleCategorySpinnerItemSelected(0);

                }
            }

            @Override
            public void onFailure(Call<PlayersResponse> call, Throwable t) {

            }
        });


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void handleCategorySpinnerItemSelected(int position) {

        ArrayList<Players> list = new ArrayList<>();
        list.clear();

        switch (position) {

            case 0:
                list.addAll(forwards);
                list.addAll(centers);
                list.addAll(defenses);
                list.addAll(goalkeepers);
                list.addAll(coaches);

            case 1:
                list.addAll(forwards);
                break;

            case 2:
                list.addAll(centers);
                break;

            case 3:
                list.addAll(defenses);
                break;

            case 4:
                list.addAll(goalkeepers);
                break;

            case 5:
                list.addAll(coaches);
        }

        updateGridView(list);
    }

    protected void updateGridView(ArrayList<Players> players) {
        boolean empty = players.isEmpty();
        gridPlayer.setVisibility(empty ? View.GONE : View.VISIBLE);
        barView.setVisibility(empty ? View.GONE : View.VISIBLE);

        if (!empty) {

            GrigPlayerAdapter playerAdapter = new GrigPlayerAdapter(context, players, getFragmentManager(), FragmentPlayers.this);
            gridPlayer.setAdapter(playerAdapter);
        }
    }

    @OnClick(R.id.btn_filter)
    public void onViewClicked() {

        ((MainActivity)getActivity()).openDrawer();

    }
}
