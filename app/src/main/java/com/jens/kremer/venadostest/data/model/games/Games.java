package com.jens.kremer.venadostest.data.model.games;

import com.google.gson.annotations.SerializedName;

public class Games {
    @SerializedName("local")
    private boolean Local;
    @SerializedName("opponent")
    private String opponent;
    @SerializedName("opponent_image")
    private String opponent_image;
    @SerializedName("datetime")
    private String datetime;
    @SerializedName("league")
    private String league;
    @SerializedName("image")
    private String image_url;
    @SerializedName("home_score")
    private int home_score;
    @SerializedName("away_score")
    private int away_score;

    public Games(boolean Local, String opponent, String opponent_image, String datetime, String league, String image_url, int home_score, int away_score) {
        this.Local = Local;
        this.opponent = opponent;
        this.opponent_image = opponent_image;
        this.datetime = datetime;
        this.league = league;
        this.image_url = image_url;
        this.home_score = home_score;
        this.away_score = away_score;
    }

    public boolean Local() {
        return Local;
    }

    public void setLocal(boolean local) {
        Local = local;
    }

    public String getOpponent() {
        return opponent;
    }

    public void setOpponent(String opponent) {
        this.opponent = opponent;
    }

    public String getOpponent_image() {
        return opponent_image;
    }

    public void setOpponent_image(String opponent_image) {
        this.opponent_image = opponent_image;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String date_time_s) {
        this.datetime = datetime;
    }

    public String getLeague() {
        return league;
    }

    public void setLeague(String league) {
        this.league = league;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getHome_score() {
        return home_score;
    }

    public void setHome_score(int home_score) {
        this.home_score = home_score;
    }

    public int getAway_score() {
        return away_score;
    }

    public void setAway_score(int away_score) {
        this.away_score = away_score;
    }
}
