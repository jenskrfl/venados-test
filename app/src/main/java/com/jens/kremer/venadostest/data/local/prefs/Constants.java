package com.jens.kremer.venadostest.data.local.prefs;

public class Constants {
    public static final long TIME_READ_CONNECTION = 60; //seconds
    public static final long TIME_OUT_CONNECTION = 60; //seconds

    public static final String BASE_URL = "https://venados.dacodes.mx/api/";
    public static final String NAME_SHAREDPREFERENCES = "prefs-venadosfc";
    public static final String STRING_GAME_LIST = "GAME_LIST";
    public static final String STRING_STATS_LIST = "STATS_LIST";
    public static final String STRING_DATA_TEAMS = "DATA_TEAMS";
    public static final String STRING_PLAYER = "PLAYER";
    public static final String noConnException = "NO INTERNET";


}
