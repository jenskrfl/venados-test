package com.jens.kremer.venadostest.data.model.players;

import com.google.gson.annotations.SerializedName;

public class Players {
    @SerializedName("name")
    private String name;
    @SerializedName("first_surname")
    private String firstSurName;
    @SerializedName("second_surname")
    private String secondSurName;
    @SerializedName("birthday")
    private String birthday;
    @SerializedName("birth_place")
    private String birthPlace;
    @SerializedName("weight")
    private double weight;
    @SerializedName("height")
    private double height;
    @SerializedName("position")
    private String position;
    @SerializedName("number")
    private int number;
    @SerializedName("position_short")
    private String positionShort;
    @SerializedName("last_team")
    private String lastTeam;
    @SerializedName("image")
    private String urlImage;
    @SerializedName("role")
    private String role;
    @SerializedName("role_short")
    private String roleShort;

    public Players(String name, String firstSurName, String secondSurName, String birthday, String birthPlace, double weight, double height, String position, int number, String positionShort, String lastTeam, String urlImage) {
        this.name = name;
        this.firstSurName = firstSurName;
        this.secondSurName = secondSurName;
        this.birthday = birthday;
        this.birthPlace = birthPlace;
        this.weight = weight;
        this.height = height;
        this.position = position;
        this.number = number;
        this.positionShort = positionShort;
        this.lastTeam = lastTeam;
        this.urlImage = urlImage;
    }

    public Players(String name, String firstSurName, String secondSurName, String birthday, String birthPlace, double weight, double height, String role, String roleShort, String urlImage) {
        this.name = name;
        this.firstSurName = firstSurName;
        this.secondSurName = secondSurName;
        this.birthday = birthday;
        this.birthPlace = birthPlace;
        this.weight = weight;
        this.height = height;
        this.urlImage = urlImage;
        this.role = role;
        this.roleShort = roleShort;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSurName() {
        return firstSurName;
    }

    public void setFirstSurName(String firstSurName) {
        this.firstSurName = firstSurName;
    }

    public String getSecondSurName() {
        return secondSurName;
    }

    public void setSecondSurName(String secondSurName) {
        this.secondSurName = secondSurName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPositionShort() {
        return positionShort;
    }

    public void setPositionShort(String positionShort) {
        this.positionShort = positionShort;
    }

    public String getLastTeam() {
        return lastTeam;
    }

    public void setLastTeam(String lastTeam) {
        this.lastTeam = lastTeam;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleShort() {
        return roleShort;
    }

    public void setRoleShort(String roleShort) {
        this.roleShort = roleShort;
    }
}
