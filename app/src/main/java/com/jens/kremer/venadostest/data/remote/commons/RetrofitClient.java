package com.jens.kremer.venadostest.data.remote.commons;



import com.jens.kremer.venadostest.BuildConfig;
import com.jens.kremer.venadostest.data.local.prefs.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient(String url){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            httpClient.readTimeout(Constants.TIME_READ_CONNECTION, TimeUnit.SECONDS);
            httpClient.connectTimeout(Constants.TIME_OUT_CONNECTION, TimeUnit.SECONDS);
            httpClient.addInterceptor(logging);
            //httpClient.addInterceptor(new ConnectivityInterceptor());
        }

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(httpClient.build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


}
