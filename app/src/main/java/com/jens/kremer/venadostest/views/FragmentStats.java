package com.jens.kremer.venadostest.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jens.kremer.venadostest.R;
import com.jens.kremer.venadostest.StatsRecyclerAdapter;
import com.jens.kremer.venadostest.data.model.stats.Stats;
import com.jens.kremer.venadostest.data.model.stats.StatsResponse;
import com.jens.kremer.venadostest.data.remote.commons.ApiUtils;
import com.jens.kremer.venadostest.data.remote.service.common.APIService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentStats extends Fragment {

    Context context;
    ArrayList<Stats> stats;
    private APIService mAPIService;

    @BindView(R.id.stats_toolbar)
    Toolbar statsToolbar;
    @BindView(R.id.recycler_stats)
    RecyclerView recyclerStats;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_stats, container, false);
        ButterKnife.bind(this, view);


        mAPIService = ApiUtils.getApiService();
        Call<StatsResponse> statsResponse = mAPIService.getStatistics();
        statsResponse.enqueue(new Callback<StatsResponse>() {
            @Override
            public void onResponse(Call<StatsResponse> call, Response<StatsResponse> response) {
                if (response.body() instanceof StatsResponse) {
                    StatsRecyclerAdapter recyclerAdapter = new StatsRecyclerAdapter(response.body().getData().getStatistics(), context);
                    recyclerStats.setAdapter(recyclerAdapter);
                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
                    recyclerStats.setLayoutManager(layoutManager);
                    recyclerStats.setHasFixedSize(true);

                }
            }

            @Override
            public void onFailure(Call<StatsResponse> call, Throwable t) {

            }
        });

        statsToolbar.setTitle("Estadísticas");
        statsToolbar.setTitleTextColor(context.getColor(R.color.textColorPrimary));
        ((AppCompatActivity)getActivity()).setSupportActionBar(statsToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        statsToolbar.setNavigationIcon(R.drawable.ic_menu_3bars);
        statsToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).openDrawer();

            }
        });
        return view;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setStats(ArrayList<Stats> stats) {
        this.stats = stats;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
}
