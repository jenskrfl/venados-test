package com.jens.kremer.venadostest;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jens.kremer.venadostest.data.model.games.Games;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class GamesRecyclerAdapter extends RecyclerView.Adapter<GamesRecyclerAdapter.MyViewHolder>{

    ArrayList<Games> games = new ArrayList<>();
    Activity activity;

    private static final int HEAD = 0;
    private static final  int LIST = 1;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        int view_type;

        private ImageView local_image;
        private TextView local_name;
        private ImageView away_image;
        private TextView away_name;
        private TextView date;
        private TextView day;
        private ImageView calendar;
        private TextView home_score;
        private TextView away_score;
        private TextView head;


        
        public MyViewHolder(View v, int viewType) {
            super(v);
            local_image = itemView.findViewById(R.id.img_games_local);
            local_name = itemView.findViewById(R.id.txt_games_home_name);
            away_image = itemView.findViewById(R.id.img_games_away);
            away_name = itemView.findViewById(R.id.txt_games_away_name);
            date = itemView.findViewById(R.id.txt_games_date);
            day = itemView.findViewById(R.id.txt_games_day);
            home_score = itemView.findViewById(R.id.txt_games_home_score);
            away_score = itemView.findViewById(R.id.txt_games_away_score);
            calendar = itemView.findViewById(R.id.img_games_caledar);
            switch (viewType){

                case LIST:
                    view_type=LIST;
                    break;

                case HEAD:
                    head = itemView.findViewById(R.id.txt_games_head);
                    view_type=HEAD;
                    break;

            }

        }
    }


    public GamesRecyclerAdapter(ArrayList<Games> games,Activity activity) {
        this.games = games;
        this.activity = activity;

    }

    @Override
    public GamesRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        if(viewType==LIST) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.games_without_head,parent,false);
            GamesRecyclerAdapter.MyViewHolder gamesViewHolder= new GamesRecyclerAdapter.MyViewHolder(v,viewType);
            return gamesViewHolder;
        }
        else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.games_with_head, parent, false);
            GamesRecyclerAdapter.MyViewHolder gamesViewHolder= new GamesRecyclerAdapter.MyViewHolder(v,viewType);
            return gamesViewHolder;

        }


    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        if(holder.view_type==LIST)
        {
            final Games game = games.get(position);

            //Check if game is local to setup the correct images for each view
            if(game.Local())
            {
                Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorPrimary).into(holder.away_image);
                holder.away_name.setText(game.getOpponent());
                Picasso.get().load("https://i.imgur.com/7xPYFef.png").into(holder.local_image);
                holder.local_name.setText("Venados F.C.");

            }else
            {
                Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorPrimary).into(holder.local_image);
                holder.local_name.setText(game.getOpponent());
                Picasso.get().load("https://i.imgur.com/7xPYFef.png").into(holder.away_image);
                holder.away_name.setText("Venados F.C.");

            }

            SimpleDateFormat simpleDateFormat =
                    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");

            // Set the date to the holder
            Date theDate = null;
            try {
                theDate = simpleDateFormat.parse(game.getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            final Date finalTheDate = theDate;
            holder.calendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    programToCalendar(finalTheDate);


                }
            });

            Calendar myCal = new GregorianCalendar();
            myCal.setTime(theDate);

            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
            Date date = null;
            try {
                date = inFormat.parse(game.getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
            String day = outFormat.format(date);
            String getDay = day.substring(0,3);



            holder.date.setText(String.valueOf(myCal.get(Calendar.DAY_OF_MONTH)));
            holder.day.setText(getDay.toUpperCase());


            holder.home_score.setText(String.valueOf(game.getHome_score()));
            holder.away_score.setText(String.valueOf(game.getAway_score()));
        }
        else if(holder.view_type==HEAD){

            final Games game = games.get(position);

            if(game.Local())
            {
                SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
                Date date = null;
                try {
                    date = inFormat.parse(games.get(position).getDatetime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat outFormat = new SimpleDateFormat("MMMM");
                String month = outFormat.format(date);

                holder.head.setText(month.toUpperCase());

                Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorPrimary).into(holder.away_image);
                holder.away_name.setText(game.getOpponent());
                Picasso.get().load("https://i.imgur.com/7xPYFef.png").into(holder.local_image);
                holder.local_name.setText("Venados F.C.");

            }else
            {

                SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
                Date date = null;
                try {
                    date = inFormat.parse(games.get(position).getDatetime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat outFormat = new SimpleDateFormat("MMMM");
                String month = outFormat.format(date);

                holder.head.setText(month.toUpperCase());

                Picasso.get().load(game.getOpponent_image()).placeholder(R.color.colorPrimary).into(holder.local_image);
                holder.local_name.setText(game.getOpponent());
                Picasso.get().load("https://i.imgur.com/7xPYFef.png").into(holder.away_image);
                holder.away_name.setText("Venados F.C.");

            }

            SimpleDateFormat simpleDateFormat =
                    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");

            // Set the date to the holder
            Date theDate = null;
            try {
                theDate = simpleDateFormat.parse(game.getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            final Date finalTheDate = theDate;
            holder.calendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    programToCalendar(finalTheDate);


                }
            });

            Calendar myCal = new GregorianCalendar();
            myCal.setTime(theDate);

            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
            Date date = null;
            try {
                date = inFormat.parse(game.getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
            String day = outFormat.format(date);
            String getDay = day.substring(0,3);



            holder.date.setText(String.valueOf(myCal.get(Calendar.DAY_OF_MONTH)));
            holder.day.setText(getDay.toUpperCase());


            holder.home_score.setText(String.valueOf(game.getHome_score()));
            holder.away_score.setText(String.valueOf(game.getAway_score()));


        }

        Picasso.get().load("https://i.imgur.com/e2XpQg8.png").into(holder.calendar);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if(games != null) {
            return games.size();
        }
        else return 0;
    }
    public  void programToCalendar(Date date)
    {
        Calendar myCal = new GregorianCalendar();
        myCal.setTime(date);
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", myCal.getTimeInMillis());
        intent.putExtra("endTime", myCal.getTimeInMillis()+90*60*1000);
        intent.putExtra("title", "Partido de Venados F.C.");
        activity.startActivity(intent);
    }
    @Override
    public int getItemViewType(int position) {
        Log.d("month2", String.valueOf(position));
        if (position == 0) {
            return HEAD;
        }
        else
        {
            String oldmonth = null;
            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
            Date date = null;
            try {
                date = inFormat.parse(games.get(position).getDatetime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat outFormat = new SimpleDateFormat("MMMM");
            String month = outFormat.format(date);

            if (position > 0) {
                SimpleDateFormat oldinFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SS:SS");
                Date olddate = null;
                try {
                    olddate = oldinFormat.parse(games.get(position - 1).getDatetime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat oldoutFormat = new SimpleDateFormat("MMMM");
                oldmonth = oldoutFormat.format(olddate);
            }


            if (month.equals(oldmonth)) {

                return LIST;


            } else {
                return HEAD;

            }
        }


    }

}
