package com.jens.kremer.venadostest.data.remote.commons;

import android.net.NetworkInfo;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectivityInterceptor implements Interceptor {

    NetworkInfo netInfo;

    public ConnectivityInterceptor(){}

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!(netInfo != null && netInfo.isConnected())) {
            throw new NoConnectivityException();
        }
        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }
}
