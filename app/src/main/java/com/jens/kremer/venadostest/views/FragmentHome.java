package com.jens.kremer.venadostest.views;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jens.kremer.venadostest.R;
import com.jens.kremer.venadostest.ViewPagerAdapter;
import com.jens.kremer.venadostest.data.model.games.Games;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FragmentHome extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    protected Context context;
    @BindView(R.id.homeToolbar)
    Toolbar homeToolbar;
    private DrawerLayout drawer;
    ViewPagerAdapter viewPagerAdapter;
    @BindView(R.id.homeTabs)
    TabLayout homeTabs;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.venadosLogo)
    ImageView venadosLogo;
    //@BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    // GamesList gamesList = new GamesList();
    ArrayList<Games> games;


    private OnFragmentInteractionListener mListener;

    public FragmentHome() {

    }

    public static FragmentHome newInstance(String param1, String param2) {
        FragmentHome fragment = new FragmentHome();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }


    }

    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        homeTabs.setupWithViewPager(viewPager);

        viewPagerAdapter = new ViewPagerAdapter(((MainActivity) getContext()).getSupportFragmentManager(), games);
        viewPager.setAdapter(viewPagerAdapter);

        Picasso.get().load(context.getString(R.string.log_url)).into(venadosLogo);

       homeToolbar.setTitle("Venados F.C");
        ((AppCompatActivity)getActivity()).setSupportActionBar(homeToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        homeToolbar.setNavigationIcon(R.drawable.ic_menu_3bars);
        homeToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).openDrawer();

            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void setGames(ArrayList<Games> games) {
        this.games = games;
    }


}
