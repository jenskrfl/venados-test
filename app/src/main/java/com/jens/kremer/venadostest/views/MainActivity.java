package com.jens.kremer.venadostest.views;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.jens.kremer.venadostest.R;
import com.jens.kremer.venadostest.data.model.games.GamesList;
import com.jens.kremer.venadostest.data.model.games.GamesResponse;
import com.jens.kremer.venadostest.data.model.players.DataTeams;
import com.jens.kremer.venadostest.data.model.players.PlayersResponse;
import com.jens.kremer.venadostest.data.model.stats.ListStats;
import com.jens.kremer.venadostest.data.model.stats.StatsResponse;
import com.jens.kremer.venadostest.data.remote.commons.ApiUtils;
import com.jens.kremer.venadostest.data.remote.service.common.APIService;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.sql.Types.TIME;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    private static Context context;

    private APIService mAPIService;

    public  GamesList gamesList = new GamesList();
    public  ListStats listStats = new ListStats();
    public  DataTeams dataTeams = new DataTeams();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_main);

        //Splash

        getdata();


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        ImageView imageView = (ImageView)hView.findViewById(R.id.imageView);
        Picasso.get().load(getString(R.string.log_url)).into(imageView);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentHome currentFragmentHome = (FragmentHome) getSupportFragmentManager().findFragmentByTag("homeFragment");
            if (currentFragmentHome.isVisible()) {
                finish();
            } else if (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();

            } else {
                super.onBackPressed();
            }
        }

        if (getSupportFragmentManager().findFragmentByTag("homeFragment")!= null){
        if(getSupportFragmentManager().findFragmentByTag("homeFragment").isVisible()){
            navigationView.getMenu().getItem(0).setChecked(true);
        }}

        if (getSupportFragmentManager().findFragmentByTag("fragmentStats") != null){
        if(getSupportFragmentManager().findFragmentByTag("fragmentStats").isVisible()){
            navigationView.getMenu().getItem(1).setChecked(true);
        }}

        if(getSupportFragmentManager().findFragmentByTag(" fragmentPlayers") != null){
        if(getSupportFragmentManager().findFragmentByTag(" fragmentPlayers").isVisible()){
            navigationView.getMenu().getItem(2).setChecked(true);
        }}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){

            case R.id.nav_home:
            setFragment(0);
            return closeDrawer();

            case R.id.nav_stats:
            setFragment(1);
            return closeDrawer();

            case R.id.nav_players:
            setFragment(2);
            return closeDrawer();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void setFragment(int position){

        FragmentHome currentFragmentHome = (FragmentHome) getSupportFragmentManager().findFragmentByTag("homeFragment");

        switch (position){

            case 0:
                if (currentFragmentHome.isVisible()) { drawer.closeDrawer(GravityCompat.START); }
                else {
                FragmentHome fragmentHome = new FragmentHome();
                replaceFragment(fragmentHome, "homeFragment");
            }
            break;

            case 1:
                FragmentStats fs = (FragmentStats) getSupportFragmentManager().findFragmentByTag("fragmentStats");
                if (fs!= null && fs.isVisible()) { drawer.closeDrawer(GravityCompat.START); }
                else {
                    FragmentStats fragmentStats = new FragmentStats();
                    fragmentStats.setStats(listStats.getStatistics());
                    replaceFragment(fragmentStats, "fragmentStats");
                }
                break;

            case 2:FragmentPlayers fp = (FragmentPlayers) getSupportFragmentManager().findFragmentByTag(" fragmentPlayers");
                if (fp!= null && fp.isVisible()) { drawer.closeDrawer(GravityCompat.START); }
                else {
                    FragmentPlayers fragmentPlayers = new FragmentPlayers();
                    //fragmentStats.setStats(listStats.getStatistics());
                    replaceFragment(fragmentPlayers, " fragmentPlayers");
                }
                break;
                default:


        }

    }

    public void setDrawerLocked(boolean enabled) {
        if (enabled) {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else {
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }


    public void replaceFragment (Fragment fragment, String tag){
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment=getSupportFragmentManager().findFragmentById(R.id.frameLayout);
        boolean fragmentPopped = getSupportFragmentManager().popBackStackImmediate (backStateName, 0);

        if (!fragmentPopped){ //fragment not in back stack, create it.
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if(currentFragment != null){
                ft.hide(currentFragment);
            }
            ft.add(R.id.frameLayout, fragment, tag);
            ft.addToBackStack(backStateName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            ft.commit();
        }

    }


    void getdata (){
        mAPIService = ApiUtils.getApiService();
        Call<StatsResponse> statsResponse = mAPIService.getStatistics();
        statsResponse.enqueue(new Callback<StatsResponse>() {
            @Override
            public void onResponse(Call<StatsResponse> call, Response<StatsResponse> response) {
                if (response.body() instanceof StatsResponse) {

                    listStats = response.body().getData();

                }
            }

            @Override
            public void onFailure(Call<StatsResponse> call, Throwable t) {

            }
        });
        Call<PlayersResponse> playersResponse = mAPIService.getPlayers();
        playersResponse.enqueue(new Callback<PlayersResponse>() {
            @Override
            public void onResponse(Call<PlayersResponse> call, Response<PlayersResponse> response) {
                if (response.body() instanceof PlayersResponse) {

                    dataTeams = response.body().getData_teams();
                }
            }

            @Override
            public void onFailure(Call<PlayersResponse> call, Throwable t) {

            }
        });
        final Call<GamesResponse> gamesResponse = mAPIService.getGames();
        gamesResponse.enqueue(new Callback<GamesResponse>() {
            @Override
            public void onResponse(Call<GamesResponse> call, final Response<GamesResponse> response) {

                if (response.body() instanceof GamesResponse) {

                    gamesList = response.body().getData();

                    if(getSupportFragmentManager().findFragmentByTag("homeFragment") == null) {
                        FragmentHome fragmentHome = new FragmentHome();
                        fragmentHome.setGames(gamesList.getGames());
                        replaceFragment(fragmentHome, "homeFragment");
                        navigationView.getMenu().getItem(0).setChecked(true);
                    }

                }

            }

            @Override
            public void onFailure(Call<GamesResponse> call, Throwable t) {

            }
        });
    }

    private boolean closeDrawer()
    {
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public GamesList getGamesList() {
        return gamesList;
    }

    public void openDrawer(){
        DrawerLayout drawerLayout  = (DrawerLayout) this.findViewById(R.id.drawer_layout);
        drawerLayout.openDrawer(Gravity.LEFT);
    }
}
