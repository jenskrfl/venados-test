package com.jens.kremer.venadostest.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.widget.ImageView;

import com.jens.kremer.venadostest.R;
import com.squareup.picasso.Picasso;

public class Splash extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    ImageView imageView  = findViewById(R.id.img_splash);

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.splash_screen);

        Picasso.get().load("https://i.imgur.com/7xPYFef.png");

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(Splash.this,Menu.class);
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}