package com.jens.kremer.venadostest.data.model.players;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Positions {
    @SerializedName("forwards")
    private ArrayList<Players> forwards = new ArrayList<>();
    @SerializedName("centers")
    private ArrayList<Players> centers= new ArrayList<>();;
    @SerializedName("defenses")
    private ArrayList<Players> defenses= new ArrayList<>();;
    @SerializedName("goalkeepers")
    private ArrayList<Players> goalKeepers= new ArrayList<>();;
    @SerializedName("coaches")
    private ArrayList<Players> coaches= new ArrayList<>();

    public Positions(ArrayList<Players> forwards, ArrayList<Players> centers, ArrayList<Players> defenses, ArrayList<Players> goalKeepers, ArrayList<Players> coaches) {
        this.forwards = forwards;
        this.centers = centers;
        this.defenses = defenses;
        this.goalKeepers = goalKeepers;
        this.coaches = coaches;
    }

    public ArrayList<Players> getForwards() {
        return forwards;
    }

    public void setForwards(ArrayList<Players> forwards) {
        this.forwards = forwards;
    }

    public ArrayList<Players> getCenters() {
        return centers;
    }

    public void setCenters(ArrayList<Players> centers) {
        this.centers = centers;
    }

    public ArrayList<Players> getDefenses() {
        return defenses;
    }

    public void setDefenses(ArrayList<Players> defenses) {
        this.defenses = defenses;
    }

    public ArrayList<Players> getGoalKeepers() {
        return goalKeepers;
    }

    public void setGoalKeepers(ArrayList<Players> goalKeepers) {
        this.goalKeepers = goalKeepers;
    }

    public ArrayList<Players> getCoaches() {
        return coaches;
    }

    public void setCoaches(ArrayList<Players> coaches) {
        this.coaches = coaches;
    }
}
