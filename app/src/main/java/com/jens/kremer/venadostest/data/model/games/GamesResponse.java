package com.jens.kremer.venadostest.data.model.games;

import com.google.gson.annotations.SerializedName;

public class GamesResponse {
    //The web service response with this 3 items.
    @SerializedName("sucess")
    private boolean succes;
    @SerializedName("data")
    private GamesList gamesList;
    @SerializedName("code")
    private int code;

    public GamesResponse(boolean succes, GamesList gamesList, int code) {
        this.succes = succes;
        this.gamesList = gamesList;
        this.code = code;
    }

    public boolean isSucces() {
        return succes;
    }

    public void setSucces(boolean succes) {
        this.succes = succes;
    }

    public GamesList getData() {
        return gamesList;
    }

    public void setData(GamesList gamesList) {
        this.gamesList = gamesList;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
